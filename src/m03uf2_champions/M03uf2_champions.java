package m03uf2_champions;

import java.util.Scanner;

/**
 *
 * @author Marc Nicolau
 */
public class M03uf2_champions {

    static Scanner lector;

    public static void main(String[] args) {
        lector = new Scanner(System.in);
        int minima = lector.nextInt();
        int jugadors = lector.nextInt();
        boolean trobat = false;
        float mitjana = 0.0f;
        int count = 0;
        
        while (jugadors != -1 && !trobat) {
            count++;
            mitjana = mitjanaAlcades(jugadors);
            if (mitjana >= minima) {
                trobat = true;
            } else {
                jugadors = lector.nextInt();
            }
        }
        if (trobat) {
            System.out.printf("%d %.2f\n", count, mitjana);
        } else {
            System.out.println("NO S'HA TROBAT CAP ALINEACIÓ");
        }
    }

    private static float mitjanaAlcades(int jugadors) {
        float mitjana = 0.0f;
        for (int i = 0; i < jugadors; i++) {
            mitjana += lector.nextInt();            
        }
        return mitjana / jugadors;
    }

}
