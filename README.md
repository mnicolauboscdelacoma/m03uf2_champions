# Champions League #

Davant el transcendental partit d’Europa League contra el Nàpols, l'entrenador del Barça, en Xavi Hernández, està pensant diferents alineacions que li donin certa garantia en l'aspecte defensiu, ja que el fet de jugar molt ofensivament el fa patir. 

La seva obsessió és tenir una línia defensiva que tingui la màxima alçada mitjana, tenint en compte una alçada mínima que és la que creu que ha de cobrir. Ha pensat diferents combinacions de defenses i les ha escrit en un paper. 

Pots ajudar-lo a decidir quins defenses ha d'alinear? 

L'**entrada del programa** estarà formada per una primera línia amb un valor que indicarà una alçada (l'alçada que vol cobrir, en cms). A continuació hi haurà diverses línies, on cadascuna d'elles contindrà diferents valors enters separats per un espai en blanc. El primer d'aquests valors indicarà quants defenses té aquella alineació defensiva, i tot seguit hi haurà les alçades (en cms) dels diferents defenses. La quantitat d'alineacions defensives és variable i sabrem que no n'hi ha més quan es llegeixi una línia que contingui exactament el valor -1. 

La **sortida del programa** estarà formada per una línia que indicarà quina és la primera alineació que troba que cobreix l'alçada indicada (un número entre 1 i N, on N és la quantitat d'alineacions llegides) seguit de la mitjana d'alçades d'aquella alineació. En cas de no trobar cap alineació que cobreixi l'alçada indicada amb l'entrada, caldrà escriure NO S'HA TROBAT CAP ALINEACIÓ.

## Exemple d'entrada
>        180 
>        3 175 178 181 
>        4 168 170 190 170
>        3 179 180 182 
>        -1 


## Exemple de sortida
>        3 180.33
